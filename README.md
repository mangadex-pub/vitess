# Vitess

See [README.orig.md](README.orig.md) for the original project readme.

This repository builds specific tags of Vitess' upstream Docker images that are available but unpublished.

Specifically the Percona MySQL server based MySQL 8.0 flavour.

Published images are based on vitess/lite's upstream:

```
registry.gitlab.com/mangadex-pub/vitess/lite/percona80:v14.0.0-mangadex-$COMMIT_SHA
registry.gitlab.com/mangadex-pub/vitess/lite/percona80:v14.0.0-mangadex-$COMMIT_SHA-YYYYmmdd-HHMM
```
